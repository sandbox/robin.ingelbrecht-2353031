Description
-----------
An awesome jQuery plugin that unleashes a Raptor of Jurassic proportions... 
Well, technically it's Cretaceous proportions, but we'll let that slide for now.
Installation
------------
To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the entire directory and all its contents to your modules directory.

3. Download the raptorize library from http://zurb.com/playground/uploads/upload/upload/95/Raptorize-Kit.zip
and place it in sites/all/libraries. Rename the dicrectory to "raptorize".

4. Copy the files "raptor.png", "raptor-sound.mp3" and "raptor-sound.ogg" to the root of your site.
You need to do this because the raptorize plugin references these files hardcoded. I made a request
to make this configurable. No answer so far... 

Configuration
-------------
To enable and configure this module do the following:

1. Go to Admin -> Modules, and enable Module Filter.

2. Go to Admin -> Configuration -> User Interface -> Raptorize Settings, and make
   any necessary configuration changes. 
