/**
 * Initializes raptorize.js.
 */

(function($) {
  Drupal.behaviors.raptorize = {
    attach: function(context, settings) {
      var settings = Drupal.settings.raptorize;

      if(settings.loadOptions.on_load){
        $(document).raptorize({
          'enterOn': 'timer',
          'delayTime': settings.delayTime
        });
      }

      if(settings.loadOptions.konami){
        $(document).raptorize({
          'enterOn': 'konami-code'
        });
      }
    }
  };
})(jQuery);
