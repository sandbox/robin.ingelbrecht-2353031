<?php

/**
 * Functionality for the admin views.
 */

/**
 * Implements hook_form().
 */
function raptorize_js_settings_form() {
  $settings = variable_get('raptorize_js_settings', array());
  $form = array();

  $options = array(
    1 => t('All pages except those listed'),
    2 => t('Only the listed pages'),
  );
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog',
    '%blog-wildcard' => 'blog/*',
    '%front' => '<front>'
      ));

  $form['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Load Raptorize.js on specific pages'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => !empty($settings['visibility']) ? $settings['visibility'] : 1,
  );
  $form['pages'] = array(
    '#type' => 'textarea',
    '#title' => '<span class="element-invisible">' . t('Pages') . '</span>',
    '#default_value' => !empty($settings['pages']) ? $settings['pages'] : NULL,
    '#description' => $description,
  );

  $form['load_options'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Load options'),
    '#options' => array(
      'on_load' => t('Run Raptorize.js on page load.'),
      'konami' => t('React on Konami Code (&uarr; &uarr; &darr; &darr; &larr; &rarr; &larr; &rarr; B A)'),
    ),
    '#default_value' => !empty($settings['load_options']) ? $settings['load_options'] : array(),
    '#description' => t("If you don't choose any of these options, you will have to initialize Raptorize.js yourself."),
  );

  $form['delay_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay time'),
    '#required' => TRUE,
    '#default_value' => !empty($settings['delay_time']) ? $settings['delay_time'] : 0,
    '#description' => t('In miliseconds'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function raptorize_js_settings_form_submit($form, &$form_state) {
  variable_set('raptorize_js_settings', array(
    'visibility' => $form_state['values']['visibility'],
    'pages' => $form_state['values']['pages'],
    'load_options' => $form_state['values']['load_options'],
    'delay_time' => $form_state['values']['delay_time'],
  ));

  drupal_set_message(t('Your settings have been saved.'));
}
